<?php

declare(strict_types=1);

namespace Logotron;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class LokiHandler extends AbstractProcessingHandler
{
    /** @var string */
    private $authority;

    public function __construct(string $authority, int $level = Logger::DEBUG, bool $bubble = true)
    {
        $this->authority = $authority;

        parent::__construct($level, $bubble);
    }

    /**
     * @inheritDoc
     */
    protected function write(array $record): void
    {
        $timestamp = (int)(\microtime(true) * 10**9);

        $data = ['streams' =>
            [
                [
                    'stream' => $record['formatted']['labels'],
                    'values' => [
                        [
                            (string)$timestamp,
                            \json_encode($record['formatted']['fields'], JSON_THROW_ON_ERROR, 512)
                        ]
                    ]
                ]
            ]
        ];

        $content = \json_encode($data, JSON_THROW_ON_ERROR, 512);

        $ctx = \stream_context_create(['http' => [
            'method'  => 'POST',
            'header'  => 'Content-Type: application/json',
            'content' => $content
        ]]);

        \file_get_contents($this->authority . '/loki/api/v1/push', false, $ctx);
    }
}
