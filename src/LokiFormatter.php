<?php

declare(strict_types=1);

namespace Logotron;

use Monolog\Formatter\NormalizerFormatter;

class LokiFormatter extends NormalizerFormatter
{
    /**
     * @var string
     */
    private $application;

    /**
     * @var string
     */
    private $environment;

    public function __construct(string $application, string $environment)
    {
        $this->application = $application;
        $this->environment = $environment;
        parent::__construct();
    }

    public function format(array $record)
    {
        $record = parent::format($record);
        $data = ['fields' => [], 'labels' => []];

        foreach ($record['context'] as $key => $value) {
            $data['fields'][$key] = $value;
        }

        foreach ($record['extra'] as $key => $value) {
            $data['fields'][$key] = $value;
        }

        $data['fields']['channel'] = $record['channel'];
        $data['fields']['message'] = $record['message'];

        $data['labels']['application'] = $this->application;
        $data['labels']['environment'] = $this->environment;
        $data['labels']['level'] = \strtolower($record['level_name']);

        return $data;
    }
}
