<?php

declare(strict_types=1);

use Logotron\LokiFormatter;
use Logotron\LokiHandler;
use Monolog\Logger;

(static function(string $environment) {
    require_once __DIR__ . '/../vendor/autoload.php';

    $handler = new LokiHandler('http://localhost:3100');
    $handler->setFormatter(new LokiFormatter('command', $environment));

    $logger = new Logger('general');
    $logger->pushHandler($handler);

    $logger->warning('the message line', ['user' => 'anon']);
})($argv[1] ?? 'development');
