<?php

declare(strict_types=1);

use Logotron\LokiFormatter;
use Logotron\LokiHandler;
use Monolog\Logger;

(static function(int $delay) {
    require_once __DIR__ . '/../vendor/autoload.php';

    $handler = new LokiHandler('http://loki:3100');
    $handler->setFormatter(new LokiFormatter('producer', 'development'));

    $logger = new Logger('general');
    $logger->pushHandler($handler);

    while (true) {
        $logger->warning('the message line', ['user' => 'anon']);
        usleep($delay);
    }
})((int) ($argv[1] ?? 0));
